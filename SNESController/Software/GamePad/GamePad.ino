/*
 Name:		GamePad.ino
 Created:	30.09.2019 16:16:22
 Author:	Thk
*/

#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>
#include "secrets.h"
#ifdef __AVR__

#include <avr/power.h>
#endif
const int buttonPin = D1;     // the number of the pushbutton pin
const int ledRGBG = D0;      // the number of the LED pin
const int ledws = D5;
long rainbowC = 0;
long lastTime = 0;
long speed = 20;
long Blink = 2000;
long last_Blink = 0;
uint8_t Helligkeit = 30;
bool toggle = true;
String header;
unsigned long currentTime = millis();
unsigned long previousTime = 0;
// Define timeout time in milliseconds (example: 2000ms = 2s)
const long timeoutTime = 2000;

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
#define PIN D4


Adafruit_NeoPixel strip = Adafruit_NeoPixel(18, PIN, NEO_GRB + NEO_KHZ800);
WiFiServer server(80);

void setup() {
	// initialize the LED pin as an output:
	pinMode(ledRGBG, OUTPUT);
	pinMode(ledws, OUTPUT);
	// initialize the pushbutton pin as an input:
	pinMode(buttonPin, INPUT_PULLUP);
	digitalWrite(ledRGBG, HIGH);
	digitalWrite(ledws, LOW);
	Serial.begin(115200);
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}
	strip.begin();
	strip.show();

	Serial.print("Connecting to ");


	WiFi.mode(WIFI_AP);           //Only Access point
	WiFi.softAP(SECRET_SSID, SECRET_PASS);  //Start HOTspot removing password will disable security
	Serial.print("HotSpt IP:");
	Serial.print("Setting soft-AP configuration ... ");
	Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");


	Serial.print("Setting soft-AP ... ");
	Serial.println(WiFi.softAP(SECRET_SSID, SECRET_PASS) ? "Ready" : "Failed!");

	Serial.print("Soft-AP IP address = ");
	Serial.println(WiFi.softAPIP());
	IPAddress myIP = WiFi.softAPIP(); //Get IP address
	Serial.println(myIP);

	

	server.begin();                           // Actually start the server
	Serial.println("HTTP server started");
}

void loop() {
	// read the state of the pushbutton value:
	handleWeb();
	if (millis() - lastTime > speed) {
		strip.setBrightness(Helligkeit);
		lastTime = millis();
		rainbowCycle(1);
	}

	if (millis() - last_Blink > Blink) {
		last_Blink = millis();
		if (toggle == true) {
			digitalWrite(ledws, HIGH);
			toggle = false;
		}
		else {
			digitalWrite(ledws, LOW);
			toggle = true;
		}
	}

	delay(5);
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(int Anzahl) {
	uint16_t i, j;


	for (i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + rainbowC) & 255));
	}
	strip.show();
	rainbowC++;
	if (rainbowC >= Anzahl * 256)
	{
		rainbowC = 0;
	}

}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
	WheelPos = 255 - WheelPos;
	if (WheelPos < 85) {
		return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if (WheelPos < 170) {
		WheelPos -= 85;
		return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}


void handleWeb() {
	WiFiClient client = server.available();   // Listen for incoming clients

	if (client) {                             // If a new client connects,
		Serial.println("New Client.");          // print a message out in the serial port
		String currentLine = "";                // make a String to hold incoming data from the client
		currentTime = millis();
		previousTime = currentTime;
		while (client.connected() && currentTime - previousTime <= timeoutTime) { // loop while the client's connected
			currentTime = millis();
			if (client.available()) {             // if there's bytes to read from the client,
				char c = client.read();             // read a byte, then
				Serial.write(c);                    // print it out the serial monitor
				header += c;
				if (c == '\n') {                    // if the byte is a newline character
				  // if the current line is blank, you got two newline characters in a row.
				  // that's the end of the client HTTP request, so send a response:
					if (currentLine.length() == 0) {
						// HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
						// and a content-type so the client knows what's coming, then a blank line:
						client.println("HTTP/1.1 200 OK");
						client.println("Content-type:text/html");
						client.println("Connection: close");
						client.println();

						// turns the GPIOs on and off
						if (header.indexOf("GET /prg") >= 0) {
							
						}
						else if (header.indexOf("GET /speed+") >= 0) {
						speed += 10;
						}
						else if (header.indexOf("GET /speed-") >= 0) {

							speed -= 10;
							if (speed < 20) {
								speed = 20;
							}
						}
						else if (header.indexOf("GET /light+") >= 0) {
							Helligkeit += 10;
							if (Helligkeit > 250) {
								Helligkeit = 250;
							}
						}
						else if (header.indexOf("GET /light-") >= 0) {
							Helligkeit -= 10;
							if (Helligkeit < 10) {
								Helligkeit = 10;
							}
						}
						// Display the HTML web page
						client.println("<!DOCTYPE html><html>");
						client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
						client.println("<link rel=\"icon\" href=\"data:,\">");
						// CSS to style the on/off buttons 
						// Feel free to change the background-color and font-size attributes to fit your preferences
						client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
						client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
						client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
						client.println(".button2 {background-color: #77878A;}</style></head>");

						// Web Page Heading
						client.println("<body><h1>ESP8266 Web Server</h1>");

						// Display current state, and ON/OFF buttons for GPIO 5  

						// If the output5State is off, it displays the ON button       
						
						client.println("<p><a href=\"/speed+\"><button class=\"button\">AUTO CHANGE TIME +</button></a></p>");


						client.println("<p><a href=\"/speed-\"><button class=\"button\">AUTO CHANGE TIME -</button></a></p>");
						client.println("<h1>" + String(speed) + " ms</h1>");
						client.println("<p><a href=\"/light+\"><button class=\"button\">Helligkeit +</button></a></p>");

						client.println("<p><a href=\"/light-\"><button class=\"button\">Helligkeit -</button></a></p>");
						client.println("<h1>" + String(Helligkeit) + "</h1>");
						// Display current state, and ON/OFF buttons for GPIO 4  

						client.println("</body></html>");

						// The HTTP response ends with another blank line
						client.println();
						// Break out of the while loop
						break;
					}
					else { // if you got a newline, then clear currentLine
						currentLine = "";
					}
				}
				else if (c != '\r') {  // if you got anything else but a carriage return character,
					currentLine += c;      // add it to the end of the currentLine
				}
			}
		}
		// Clear the header variable
		header = "";
		// Close the connection
		client.stop();
		Serial.println("Client disconnected.");
		Serial.println("");




	}
}
