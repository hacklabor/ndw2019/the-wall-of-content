# The Big SNES Controller
![Drawing](SNESController/Images/SNESC_HELL1.jpg "")
![Drawing](SNESController/Images/SNESC_Beleuchtet.jpg "")

## Parts

- 2 Regaleinlegeböden 50x80cm weiß
- 1 Set Taster 60mm https://www.amazon.de/gp/product/B01MSNXLN0/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1
- 1 Wireless controller https://www.amazon.de/iNNEXT-Controller-USB-Empf%C3%A4nger-Nintendo-Spiele-Unterst%C3%BCtzung/dp/B07FTCWBSY
- 1 x Wemos mini D1 (ESP8266) https://www.amazon.de/dp/B076F81VZT/ref=cm_sw_em_r_mt_dp_U_mt0ODb5G6Y15M
- 1 x Wemos mini Spannungversorgung https://www.amazon.de/gp/product/B07F6F4KVK/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1
- 1m WS2811 12V RGB https://www.amazon.de/dp/B01CNL6LBK/ref=cm_sw_em_r_mt_dp_U_Cs0ODbR16VAR4
- Litze
- Sprühfarbe Grundierung, Silbergrau, Schwarz, Lack glänzend
- Spachtel, Schleifpapier verschiedene Körnungen

## Zusammenbau

- Umrisse Übertragen und Platte ausschneiden
- Löcher für Taster erstellen 22,5mm
- Farbgestalltung nach Vorlage danach lackieren
- SNES wirelees Controller zerlegen und Kabel an die Lötpunkte neben den Kontaktflächen der originalen Tasten anlöten
- Diese Kabel mit den Microschaltern der 60mm Buttons verbinden, weißer Button == Start !!! Wichtig Start schaltet den Wirelees Controller ein
- Wemos programmieren und mit RGB LED verbinden
- Buttonbeleuchtung entweder fest auf 12V oder über ESP Ausgang dann Open Collector Treiber verwenden ULN2003
- Funktionstest mit 12V (wir haben einen 3 Zellen Lipo mit 1350mAh genommen)
- In der mitte Abstandslatten ( 2 Dachlatte 300mm lang) montieren. diese dienen Gleichzeitig als gehäuse für den Wirelees Controller und Wemos
- Daran die Zweite Platte als Ständer montieren