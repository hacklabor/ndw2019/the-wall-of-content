# The Wall of Content

## wie wird Content erstellt

Bildabmessungen Grundabmessung 1080x1920

![Drawing](/images/Schwarzmitmaßen.png "")
 
 
- Dateiname "0(Serie)-0(Bildreihenfolge)-235(Anzeigezeit in ms)-Dateiname.png"
    - z.B. "0-0-235-AwesomePicture.png", "0-1-598-CuteDogo.png" 
    - Aus Serie 0 wird Bild 0 für 235ms Angezeigt dann Bild 1 für 598ms 
- Hoher Kontrast
- Einfache Geometrie; Für Bilder / Farben hatt der Beamer nicht genug Kontrast.
- Schrift "HACK" min. 48px
- Text möglichst unten



[https://gitlab.com/hacklabor/ndw2019/the-wall-of-content/blob/master/aktuell_1920x1080Schwarz.png](https://gitlab.com/hacklabor/ndw2019/the-wall-of-content/blob/master/aktuell_1920x1080Schwarz.png "aktuelleVorlage")