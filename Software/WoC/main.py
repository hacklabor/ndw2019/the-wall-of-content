import sys

from pygame.locals import *
import requests
from random import randint
import pygame
import time
import Settings
import os
import pygame.freetype  # Import the freetype module.
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


class Food:
    x = 0
    y = 0
    step = Settings.GridSize

    def __init__(self, x, y):
        self.x = x * self.step
        self.y = y * self.step + Settings.OffsetTop

    def draw(self, surface, image):
        surface.blit(image, (self.x, self.y))


class Player:
    x = [0+ 6*Settings.GridSize]
    y = [0 + Settings.OffsetTop]
    step = Settings.GridSize
    direction = 0
    length = 3

    updateCountMax = 2
    updateCount = 0

    def __init__(self, length):
        self.x =[0]
        self.y = [0 + Settings.OffsetTop]
        self.length = length
        for i in range(0, 2000):
            self.x.append(-100)
            self.y.append(-100)

        # initial positions, no collision.
        self.x[0] = 6 * Settings.GridSize
        self.x[1] = 5 * Settings.GridSize
        self.x[2] = 4 * Settings.GridSize

    def update(self):

        self.updateCount = self.updateCount + 1
        if self.updateCount > self.updateCountMax:

            # update previous positions
            for i in range(self.length - 1, 0, -1):
                self.x[i] = self.x[i - 1]
                self.y[i] = self.y[i - 1]

            # update position of head of snake
            if self.direction == 0:
                self.x[0] = self.x[0] + self.step
            if self.direction == 1:
                self.x[0] = self.x[0] - self.step
            if self.direction == 2:
                self.y[0] = self.y[0] - self.step
            if self.direction == 3:
                self.y[0] = self.y[0] + self.step

            self.updateCount = 0

    def moveRight(self):
        self.direction = 0

    def moveLeft(self):
        self.direction = 1

    def moveUp(self):
        self.direction = 2

    def moveDown(self):
        self.direction = 3

    def draw(self, surface, image):
        for i in range(0, self.length):
            surface.blit(image, (self.x[i], self.y[i]))


class Game:

    def isCollision(self, x1, y1, x2, y2, bsize):

        if (x1 == x2) and (y1 == y2):
            return True
        return False


class Background(pygame.sprite.Sprite):
    def __init__(self, image_file, location):
        pygame.sprite.Sprite.__init__(self)  # call Sprite initializer
        self.image = pygame.image.load(image_file)
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location


class App:
    windowWidth = Settings.ScreenWidth
    windowHeight = Settings.ScreenHight
    player = 0
    food = 0

    def __init__(self):
        self.GAME_FONT = None
        self.white = (255, 255, 255)
        self.black = (0, 0, 0)
        self.red = (255, 0, 0)
        self.fileName = []
        self.PictureShowTime = 0
        self.LastPictureTime = 0
        self.blinkTime = 0
        self.blinkToggle = False
        self.ContentNr = 0
        self.FileNr = 0
        self.gameTimeDelay = Settings.StartGameTimeDelay
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self._food_surf = None
        self._contentIMG = None
        self._windowUp = None
        self._windowMiddle = None
        self._windowDown = None
        self._DeathArea = None
        self._GameOver = None
        self._PressAnyBW = None
        self._PressAnyWB = None
        self._Message = None
        self.game = Game()
        self.player = Player(Settings.SnakeLenght)
        self.food = Food(24, 7)
        self.Pkt = Settings.maxPkt
        self.gesPkt = 0
        self.highPkt = 0
        self._pktText = None
        f = open("high.txt", "r")
        s = f.read()
        f.close()
        self.highPkt = int(s)

        self.TickerCounter = 0
        self.oldMessage = ""

    def on_init(self):
        pygame.init()
        for (dirpath, dirnames, filenames) in os.walk("./content"):
            self.fileName.extend(filenames)
            break

        self.ContentNr = 0
        self.FileNr = 0
        self.LastPictureTime = 0
        self.PictureShowTime = 0
        self._display_surf = pygame.display.set_mode((self.windowWidth, self.windowHeight),
                                                     pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN)

        self._running = True
        self._image_surf = pygame.image.load("./Images/Snake.png").convert()
        self._food_surf = pygame.image.load("./Images/Food.png").convert()
        self._windowUp = pygame.image.load("./Images/FensterOben.bmp").convert()
        self._windowMiddle = pygame.image.load("./Images/FensterMitte.bmp").convert()
        self._windowDown = pygame.image.load("./Images/FensterUnten.bmp").convert()
        self._DeathArea = pygame.image.load("./Images/DeathArea.bmp").convert()
        self._GameOver = pygame.image.load("./Images/GameOver.bmp").convert()
        self._PressAnyBW = pygame.image.load("./Images/PressAnyButtonBW.bmp").convert()
        self._PressAnyWB = pygame.image.load("./Images/PressAnyButtonWB.bmp").convert()
        self._Message = pygame.image.load("./Images/messages_background.bmp").convert()
        self._food_surf = pygame.transform.rotate(self._food_surf, 90)
        self._windowUp = pygame.transform.rotate(self._windowUp, 90)
        self._windowMiddle = pygame.transform.rotate(self._windowMiddle, 90)
        self._windowDown = pygame.transform.rotate(self._windowDown, 90)
        self._DeathArea = pygame.transform.rotate(self._DeathArea, 90)
        self._GameOver = pygame.transform.rotate(self._GameOver, 90)
        self._PressAnyBW = pygame.transform.rotate(self._PressAnyBW, 90)
        self._PressAnyWB = pygame.transform.rotate(self._PressAnyWB, 90)
        self._Message = pygame.transform.rotate(self._Message, 90)
        self.GAME_FONT=pygame.font.Font("./Font/i8080.ttf", Settings.GridSize)
        pygame.mouse.set_visible(False)

    def on_event(self, event):
        if event.type == QUIT:
            self._running = False

    def ShowImage (self):
        found = False
        print("IMAGESHOW")
        for fi in self.fileName:
            fileName = fi.split("-")

            if int(fileName[0]) == self.ContentNr:
                if int(fileName[1]) == self.FileNr:
                    found = True
                    self._display_surf.fill((255, 255, 255))
                    self._contentIMG = pygame.image.load("./content/" + fi).convert()
                    self._contentIMG = pygame.transform.rotate(self._contentIMG, 90)
                    self._display_surf.blit(self._contentIMG, [0, 0])
                    self.PictureShowTime = int(fileName[2]) / 1000
                    self.LastPictureTime = time.time()
                    self.FileNr += 1
                    break
        if not found:
            self.FileNr = 0
            self.ContentNr += 1
            if self.ContentNr > 9:
                self.ContentNr = 0
                pass

    def attach_text(self, message):
        msg_lines = message.splitlines()
        print(msg_lines)
        print( self.oldMessage == message)
        print( self.oldMessage)
        print( message)
        if msg_lines[0] == "" and msg_lines[1] == "" and msg_lines[2] == "" and msg_lines[3] == "":
            print("LINES")
            self.ShowImage()
            return
        if message == self.oldMessage:
            print("OLDMESS")
            self.ShowImage()
            return
        self.oldMessage = message
        self._display_surf.fill((255, 255, 255))
        self.PictureShowTime = int(Settings.ZeitNachrichten / 1000)
        self.LastPictureTime = time.time()
        img = Image.open("./Images/messages_background.bmp")
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("./Font/Hack-Bold.ttf", Settings.FONT_SIZE)
        y_text = Settings.FROM_TOP
        for msg_line in msg_lines:
            print(y_text)
            draw.text((Settings.FROM_LEFT, y_text), msg_line, Settings.FONT_COLOR, font=font)
            y_text += Settings.FROM_TOP_STEP
            if y_text == Settings.FROM_TOP + (3 * Settings.FROM_TOP_STEP):
                y_text += Settings.DIVIDER_STEP
        mode = img.mode
        size = img.size
        data = img.tobytes()
        self._Message = pygame.image.fromstring(data, size, mode)
        self._Message = pygame.transform.rotate(self._Message, 90)
        self._display_surf.blit(self._Message, [0, 0])
        return

    def Fenster(self, x1, y1):

        x1 = int(x1 / Settings.GridSize)
        y1 = int(y1 / Settings.GridSize)

        if y1 > 15 or y1 < 7:
            return True
        if x1 < 3 or x1 > 21:
            return True
        if 11 > x1 > 5:
            return True
        if 20 > x1 > 13:
            return True
        return False

    def on_loop(self):
        self.player.update()

        # does snake eat food?
        for i in range(0, self.player.length):
            if self.game.isCollision(self.food.x, self.food.y, self.player.x[i], self.player.y[i], 40):

                while 1:
                    self.food.x = randint(0, 27) * Settings.GridSize
                    self.food.y = randint(0, 18) * Settings.GridSize + Settings.OffsetTop
                    result = self.Fenster(self.food.x, self.food.y)
                    if result:
                        print("Good Food Place")
                        break
                self.gesPkt += self.Pkt
                self.Pkt = Settings.maxPkt
                self.player.length = self.player.length + 1
        if (self.player.x[0] / Settings.GridSize > 27) | (self.player.x[0] / Settings.GridSize < 0):
            return True

        if (self.player.y[0] / Settings.GridSize > 18) | (self.player.y[0] / Settings.GridSize < 0):
            return True

        result = self.Fenster(self.player.x[0], self.player.y[0])
        if not result:
            return True
        # does snake collide with itself?
        for i in range(2, self.player.length):
            if self.game.isCollision(self.player.x[0], self.player.y[0], self.player.x[i], self.player.y[i], 40):
                return True

        return False
        pass

    def on_render(self, mode):

        if time.time()-self.blinkTime > Settings.BlinkChangeTime:
            self.blinkTime = time.time()
            self.blinkToggle = not self.blinkToggle

        if (mode == 1) or (mode == 2):
            self._display_surf.fill((255, 255, 255))
            self._display_surf.blit(self._windowUp, Settings.PosWindowTop)
            self._display_surf.blit(self._windowMiddle, Settings.PosWindowMiddle)
            self._display_surf.blit(self._windowDown, Settings.PosWindowDown)
            self._display_surf.blit(self._DeathArea, Settings.PosDeathArea)
            self.GAME_FONT = pygame.font.Font("./Font/i8080.ttf", Settings.GridSize + 5)
            Text = "HIGH:"+str("%07d" % self.highPkt)+" NOW:" + str("%07d" % self.gesPkt)+ " P:"+str("%03d" % self.Pkt)
            self._pktText = self.GAME_FONT.render(Text, True, self.black, self.white)
            self._pktText = pygame.transform.rotate(self._pktText, 90)
            self._display_surf.blit(self._pktText, (1620-5 - Settings.GridSize, 0))

            # self.GAME_FONT = pygame.font.Font("./Font/Hack-Bold.ttf", Settings.GridSize)
            #
            #
            # if len(self.TickerText) < 31:
            #     self.TickerText += " " + Settings.defaultText
            # Text = self.TickerText[0:31]
            #
            #
            #
            # if self.TickerCounter > 3:
            #     self.TickerText = self.TickerText[1:]
            #     self.TickerCounter = 0
            #
            # self.TickerCounter += 1
            #
            # self._pktText = self.GAME_FONT.render(Text, True, self.black, self.white)
            # self._pktText = pygame.transform.rotate(self._pktText, 90)
            # self._display_surf.blit(self._pktText, (1620 - 5 - Settings.GridSize, 0))

            #pygame.draw.line(self._display_surf, self.black, (1620+5-2 * Settings.GridSize, 0), (1620+5-2* Settings.GridSize, 1080), 5)
            pygame.draw.line(self._display_surf, self.black, (1620+5 - Settings.GridSize, 0), (1620+5 - Settings.GridSize, 1080), 5)
            self.player.draw(self._display_surf, self._image_surf)
            self.food.draw(self._display_surf, self._food_surf)
            if mode == 1:
                pygame.display.flip()

        if mode == 2:
            self.GAME_FONT = pygame.font.Font("./Font/i8080.ttf", 210)
            Text = "GAME OVER"
            self._pktText = self.GAME_FONT.render(Text, True, self.black, self.white)
            self._pktText = pygame.transform.rotate(self._pktText, 90)
            self._display_surf.blit(self._pktText, (380, 8))
            #self._display_surf.blit(self._GameOver, Settings.PosGameOver)

            self.GAME_FONT = pygame.font.Font("./Font/i8080.ttf",99)
            Text = "PRESS START TO PLAY"

            if self.blinkToggle:
                self._pktText = self.GAME_FONT.render(Text, True, self.white, self.black)
                #self._display_surf.blit(self._PressAnyBW, Settings.PosPressAny)
            else:
                self._pktText = self.GAME_FONT.render(Text, True, self.black, self.white)
                #self._display_surf.blit(self._PressAnyWB, Settings.PosPressAny)
            self._pktText = pygame.transform.rotate(self._pktText, 90)
            self._display_surf.blit(self._pktText, (890, 11))
            pygame.display.flip()
        if mode == 3:

            if time.time()-self.LastPictureTime > self.PictureShowTime:
                try:
                    r = requests.get(url=Settings.API_URL)
                    print(r)
                    data = r.json()

                    if "message" not in data:
                        print("Empty")
                        self.ShowImage()
                    else:
                        if data['message'] == self.oldMessage:
                            r = requests.get(url=Settings.API_URL)

                            data = r.json()
                        self.attach_text(data['message'])
                except:
                    print ("Fehler Datenuebertragung")
                    print  ("Unexpected error:", sys.exc_info()[0])
                    self.ShowImage()

                pygame.display.flip()


    @staticmethod
    def on_cleanup():
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False

        mode = 1
        lastGameTime = time.time()
        while (self._running):
            pygame.event.pump()
            keys = pygame.key.get_pressed()
            for i in range(pygame.joystick.get_count()):
                joystick = pygame.joystick.Joystick(i)
                joystick.init()
                if (joystick.get_button(2)):
                    self.player.moveRight()

                if (joystick.get_button(0)):
                    self.player.moveLeft()

                if (joystick.get_button(1)):
                    self.player.moveUp()

                if (joystick.get_button(3)):
                    self.player.moveDown()
                if mode > 1:

                    if (joystick.get_button(9)) :
                        self.player = Player(Settings.SnakeLenght)
                        self.food = Food(15, 1)
                        mode = 1
                        self.gameTimeDelay = Settings.StartGameTimeDelay
                        self.player.moveRight()
                        lastGameTime = time.time()
                        self.Pkt = Settings.maxPkt
                        self.gesPkt = 0
                        self.LastPictureTime = 0


            if (keys[K_RIGHT] ):
                self.player.moveRight()

            if (keys[K_LEFT] ):
                self.player.moveLeft()

            if (keys[K_UP] ):
                self.player.moveUp()

            if (keys[K_DOWN] ):
                self.player.moveDown()
            if mode > 1:

                if (keys[K_u] ):
                    self.player = Player(Settings.SnakeLenght)
                    self.food = Food(15, 1)
                    mode = 1
                    self.gameTimeDelay = Settings.StartGameTimeDelay
                    self.player.moveRight()
                    lastGameTime = time.time()
                    self.Pkt = Settings.maxPkt
                    self.gesPkt=0
                    self.LastPictureTime = 0

            if (keys[K_ESCAPE]):
                self._running = False
            if mode == 1:
                if not self.on_loop():
                    mode = 1
                    lastGameTime = time.time()
                else:
                    if self.gesPkt > self.highPkt:
                        self.highPkt = self.gesPkt
                        f = open("high.txt", "w")
                        s = f.write(str(self.highPkt))
                        f.close()

                    mode = 2

            if (time.time()-lastGameTime) > Settings.GameOverTimout:
                self.gameTimeDelay = 1
                mode = 3

            if mode == 1:
                self.Pkt -= 1
                if self.Pkt < 1:
                    self.Pkt = 1

            self.on_render(mode)

            time.sleep(self.gameTimeDelay / 1000.0);
        self.on_cleanup()


if __name__ == "__main__":
    theApp = App()
    theApp.on_execute()
