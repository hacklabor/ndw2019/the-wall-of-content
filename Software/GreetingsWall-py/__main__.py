import requests
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

""" PATHS """
TEMPLATE_PATH = "./Images/aktuell_1920x1080Schwarz.bmp"
FONT_PATH = "ttf/Hack-Bold.ttf"

""" POSITION CONSTANTS """
FROM_TOP = 820
FROM_LEFT = 75
FROM_TOP_STEP = 85
DIVIDER_STEP = 210
FONT_SIZE = 62

""" APP CONSTANTS """
API_URL = "http://ndw.hacklabor.de/raspberry"
FONT_COLOR = (0)


def create_out_filename():
    return "output/test.png"


def attach_text(message):
    msg_lines = message.splitlines()
    img = Image.open(TEMPLATE_PATH)
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(FONT_PATH, FONT_SIZE)

    y_text = FROM_TOP
    for msg_line in msg_lines:
        draw.text((FROM_LEFT, y_text), msg_line, FONT_COLOR, font=font)
        y_text += FROM_TOP_STEP
        if y_text == FROM_TOP + (3 * FROM_TOP_STEP):
            y_text += DIVIDER_STEP

    image_out_pathname = create_out_filename()
    img.save(image_out_pathname)
    return


def create_message_image():
    r = requests.get(url=API_URL)

    data = r.json()

    if "message" not in data:
        print("Empty")
    else:
        attach_text(data['message'])
    return


if __name__ == '__main__':
    create_message_image()
