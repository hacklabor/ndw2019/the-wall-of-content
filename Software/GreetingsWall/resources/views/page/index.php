<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="https://hacklabor.de/assets/img/icon-hal.png">

    <title>Hacklabor - The Wall of Content</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/hack-font/3.003/web/fonts/hack-bold-subset.woff">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/hack-font@3/build/web/hack.css">
</head>

<body class="text-center">
<div class="container">
    <div aria-live="polite" aria-atomic="true" style="position: relative; min-height: 200px;">
        <div id="success-toast" class="toast" data-delay="4000" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="mr-auto">Nachricht gesendet</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body text-dark">
                Warte paar Minuten um sie auf der Wand zu sehen
            </div>
        </div>

        <div id="error-toast" class="toast" data-delay="4000" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="mr-auto">Fehler</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Es gab ein Problem
            </div>
        </div>
    </div>

    <form class="form-signin" id="message-form">
        <div class="row">
            <div class="col"><img class="mb-4" src="logo.png" alt="" width="288" height="288"></div>
        </div>
        <div class="row">
            <div class="col"><strong class="h4 mb-3 font-weight-normal">Deine Nachricht:</strong></div>
        </div>
        <div class="row">
            <div class="offset-3 col-6">
                <div id="blinker" class="blink_me" style="font-weight: bold; color: red; display: none;">
                  Keine Nachricht!
                </div>
                <input class="form-control mt-3 no-border message-input" id="message" name="message" maxlength="142" placeholder="Deine Nachricht ...."/><br />

                <div class="row">
                    <div class="col small">
                        <span id="length-indicator">0</span> / 142
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="offset-4 col-4 text-center">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button id="submit-btn" class="btn btn-lg btn-primary btn-block mt-4" type="button" onclick="sendMessage()">Sende</button>
                <div id="spinner" style="display: none;" class="spinner-border text-success" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <p class="mt-3 mb-3 text-muted">Nacht des Wissens 2019</p>
            </div>
        </div>
    </form>
</div>


<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<script>
    var message = $("#message");
    var submitBtn = $("#submit-btn");
    var spinner = $("#spinner");
    var blinker = $("#blinker");

    function showSpinner()
    {
        submitBtn.hide();
        spinner.show();
    }

    function hideSpinner() {
        spinner.hide();
        submitBtn.show();
    }

    function sendMessage()
    {
        showSpinner();
        var form = $("#message-form").serialize();

        if (message.val().length === 0) {
            blinker.show().delay(1000).fadeOut();
            hideSpinner();
        } else {
            $.ajax({
                type : 'POST',
                url : "/message",
                contentType : 'application/x-www-form-urlencoded',
                data : form,
                success : function(response) {
                    $('#success-toast').toast('show');
                },
                error : function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                    $('#error-toast').toast('show');
                },
                complete : function () {
                    message.val("");
                    hideSpinner();
                }
            });
        }
    }

    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $("#message").keyup(function () {
            var inputLength = $(this).val().length;
            $("#length-indicator").text(inputLength);
        });

    });
</script>

<style>
    .toast {
        position: absolute;
        top: 0;
        right: 0;
    }

    .blink_me {
        animation: blinker 0.2s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }
</style>
</body>
</html>
