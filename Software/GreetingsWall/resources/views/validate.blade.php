@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-2 offset-8">
                <a href="/message/validate-all" class="btn btn-success btn-block">Validate all</a>
            </div>
            <div class="col-2">
                <a href="/message/reject-all" href="" class="btn btn-danger btn-block">Reject all</a>
            </div>
        </div>
        <hr />
        <div class="row mt-3" style="font-weight: bold;">
            <div class="col-1">ID</div>
            <div class="col-4">Message</div>
            <div class="col-3">Created</div>
            <div class="col-4">Actions</div>
        </div>
        <hr style="border-bottom: 1px solid white;"/>
        <?php $index = 1 ?>
        <?php foreach ($messages as $message) { ?>
        <div class="row mt-1">
            <div class="col-1 small">
                <?php echo $message->id ?>
            </div>
            <div class="col-4 text-left small">
          <pre>
              <?php echo "\n" . htmlentities($message->message) ?>
          </pre>
            </div>
            <div class="col-3 small">
                <?php echo $message->created_at->diffForHumans() ?>
            </div>
            <div class="col-2 small">
                <a href="/message/<?php echo $message->id ?>/validate" class="btn btn-sm small btn-success">
                    OK
                </a>
            </div>
            <div class="col-2 small">
                <a href="/message/<?php echo $message->id ?>/remove" class="btn btn-sm small btn-danger px-2">
                    X
                </a>
            </div>
        </div>
        <?php $index += 1 ?>
        <?php } ?>
    </div>
@endsection

