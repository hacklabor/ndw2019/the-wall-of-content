<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['web'])->group(function () {
    Route::get('/', 'PageController@index')->name('home');
    Route::get('/message', 'MessageController@index');
    Route::get('/message/validate-all', 'MessageController@validateAll');
    Route::get('/message/reject-all', 'MessageController@rejectAll');
    Route::get('/message/{id}/validate', 'MessageController@validateMsgById');
    Route::get('/message/{id}/remove', 'MessageController@removeMsgById');
    Route::post('/message', 'MessageController@create');

    Route::get('/raspberry', 'MessageController@raspberry');

    Route::middleware(['auth'])->group(function () {
        Route::get('/validate', 'MessageController@validateMessages');
    });
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);
