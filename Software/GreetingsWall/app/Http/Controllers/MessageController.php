<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        return Message::all();
    }

    public function create(MessageRequest $request)
    {
//        $request = $request->all();
        $message = $request->input('message');
//        foreach ($request as $key => $msgPart) {
//            if ($key !== '_token') $message .= ($msgPart . "\n");
//        }
        $messageObject = new Message();
        $messageObject->message = $message;
        $res = $messageObject->save();
        return response()->json(['result' => $res]);
    }

    public function validateAll()
    {
        $messages = Message::where('valid', '=', false)->where('presented', '=', false)->get();
        foreach ($messages as $message)
        {
            $message->valid = true;
            $message->save();
        }
        return redirect('/validate');
    }

    public function rejectAll()
    {
        $messages = Message::where('valid', '=', false)->where('presented', '=', false)->get();
        foreach ($messages as $message)
        {
            $message->valid = false;
            $message->presented = true;
            $message->save();
        }
        return redirect('/validate');
    }

    public function validateMessages()
    {
        $messages = Message::where('presented', '=', false)->where('valid', '=', false)->orderBy('created_at', 'ASC')->get();
        return view('validate', ['messages' => $messages]);
    }

    public function validateMsgById(int $id)
    {
        $message = Message::findOrFail($id);
        $message->valid = true;
        $message->save();
        return redirect('/validate');
    }

    public function removeMsgById(int $id)
    {
        $message = Message::findOrFail($id);
        $message->valid = false;
        $message->presented = true;
        $message->save();
        return redirect('/validate');
    }

    public function raspberry()
    {
        $message = Message::where('presented', '=', false)
            ->where('valid', '=', true)
            ->orderBy('created_at', 'ASC')->first();
        if ($message) {
            $messagesToPresentNumber = Message::where('presented', '=', false)
                ->where('valid', '=', true)->count();
            if ($messagesToPresentNumber > 1) {
                $message->presented = true;
                $message->save();
            }
            return response()->json($message);
        }
        else return response()->json(null);
    }
}
