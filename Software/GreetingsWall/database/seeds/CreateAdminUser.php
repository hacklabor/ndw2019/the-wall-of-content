<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateAdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'hacklabor',
            'email' => 'hacklabor@hacklabor.de',
            'password' => bcrypt('hacklabor124'),
        ]);
    }
}
