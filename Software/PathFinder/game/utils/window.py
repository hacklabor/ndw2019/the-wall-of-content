import pyglet
from pyglet.window import key


def attach_window_events(window, graphics):
    @window.event
    def on_draw():
        window.clear()
        graphics.draw_blocks()

    @window.event
    def on_key_press(symbol, modifiers):
        if symbol == key.ENTER:
            print('The enter key was pressed.')
            pyglet.app.exit()