from config import config

POINT_SIZE = config['POINT_SIZE']


def matrix_pos_to_coordinates(row_idx, col_idx):
    return row_idx * POINT_SIZE, col_idx * POINT_SIZE


def position_to_quads(x, y):
    return x, y, x, y + POINT_SIZE, x + POINT_SIZE, y + POINT_SIZE, x + POINT_SIZE, y
