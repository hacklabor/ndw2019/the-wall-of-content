import numpy as np
from config import config

MATRIX = config['MATRIX']


def get_matrix():
    matrix = np.array(config['MATRIX'])

    # For simplicity, rotating the matrix by 270 degrees (cleaner config matrix)
    matrix = np.rot90(matrix)
    matrix = np.rot90(matrix)
    return np.rot90(matrix)


def get_matrix_dimensions():
    matrix = get_matrix()
    return matrix.shape
