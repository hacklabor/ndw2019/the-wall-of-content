import pyglet
from config import config

IMAGE_EXTENSION = '.png'


def get_filename(idx):
    time = int((1 / config['TICK']) * 1000) # in ms (milliseconds)
    return "1-"+str(idx) + '-' + str(time)+"-RandomColor" + IMAGE_EXTENSION # pattern: <idx>-<time>-<ProjectName>.png


def get_filepath(idx):
    filename = get_filename(idx)
    return 'output/' + config['IMAGE_FOLDER'] + filename


def save_frame(idx):
    file_path = get_filepath(idx)
    pyglet.image.get_buffer_manager().get_color_buffer().save(file_path)