import pyglet
from config import config
from game.utils.window import attach_window_events
from game.graphics.graphics import Graphics
from game.utils.image import save_frame


# This is the main callable action scheduled to run by the pyglet clock
def tick_action(idx):
    # Draw the black, uncrossable boxes
    if config['BLOCKS_ENABLED']:
        Graphics.draw_blocks()

    # Save current tick frame as a PNG
    save_frame(Game.tick)

    # Increment game ticker, used as image index
    Game.tick += 1


"""
GAME CLASS
Serves as the main manager of this script. Initializes the engines: graphics & path_finder and performs the initial 
event loop run for pyglet
"""


class Game:
    tick = 0

    def __init__(self):
        self._window = pyglet.window.Window(
            width=config['WINDOW_WIDTH'],
            height=config['WINDOW_HEIGHT'],
            style=pyglet.window.Window.WINDOW_STYLE_DIALOG
        )
        self._graphics = Graphics()

    def run(self):
        # Attach event listeners to pyglets main event loop
        attach_window_events(self._window, self._graphics)

        # Schedule tick action
        pyglet.clock.schedule_interval(tick_action, float(1 / config['TICK']))

        # Prepare and run pyglet
        pyglet.gl.glClearColor(1, 1, 1, 1)
        pyglet.app.run()
