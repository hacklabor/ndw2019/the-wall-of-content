import pyglet
from game.utils.position import position_to_quads


class Point:
    def __init__(self, position=None, color=(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)):
        self.position = position
        self.quads = position_to_quads(position[0], position[1])
        self.color = color

    def set_position(self, position):
        self.position = position

    def draw(self):
        pyglet.graphics.draw(4, pyglet.gl.GL_QUADS,
                             ('v2i', self.quads),
                             ('c3B', self.color))