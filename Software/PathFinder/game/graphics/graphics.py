import random
from game.graphics.models.point import Point
from game.utils.position import matrix_pos_to_coordinates
from game.utils.matrix import get_matrix, get_matrix_dimensions


class Graphics():
    def __init__(self):
        return

    @staticmethod
    def draw_blocks():
        matrix = get_matrix()
        rows, cols = get_matrix_dimensions()
        for row_idx in range(0, rows):
            for col_idx in range(0, cols):
                point_pos = matrix_pos_to_coordinates(row_idx=row_idx, col_idx=col_idx)
                if matrix[row_idx, col_idx] == 1:
                    point = Point(position=point_pos)
                    point.draw()
                else:
                    random_color_r = random.randint(0, 255)
                    random_color_g = random.randint(0, 255)
                    random_color_b = random.randint(0, 255)
                    random_color = random_color_r , random_color_g, random_color_b
                    point = Point(position=point_pos, color=(random_color + random_color + random_color + random_color))
                    point.draw()


        return

    def draw_points(self):
        return

    def draw_mesh(self):
        return