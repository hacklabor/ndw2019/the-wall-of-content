### The Wall of Content - Path Finder

#### File structure

* `__main.py__` - entrypoint
* `config.py` - app configurations as dict
* `resources` - documentation resources (not used by the app)
* `output` - output folder for all test & production images
* `game` - apps scripts


#### Mesh

![mesh](resources/mesh_plan_60x60.png)